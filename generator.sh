#!/bin/sh
# Generates ttf-malayalam-fonts packages for arch

### Edit the font details as required ###

FLAVORS=( "arch" )

FONTS=( "AnjaliOldLipi" "Suruma" "Chilanka" "Dyuthi" "Keraleeyam" "Meera" "Rachana" "RaghuMalayalamSans" "Suruma" ) 

fontdetails () {
  case $1 in 
    AnjaliOldLipi)
      echo "6.1.1" "67" "This is Anjali Old Lipi, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language." "GPL3"
      ;;
    Chilanka)
      echo "1.0" "67" "This is Chilanka, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language." "OFL1.1"
      ;;
    Dyuthi)
      echo "1.0" "67" "This is Dyuthi, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language." "GPL3"
      ;;
    Keraleeyam)
      echo "1.0_beta" "67" "This is Keraleeyam, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language." "OFL1.1"
      ;;
    Meera)
      echo "6.1.1" "65-0" "This is Meera, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language." "GPL3"
      ;;
    Rachana)
      echo "6.1.1" "65-0" "This is Rachana, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language." "GPL2"
      ;;
    RaghuMalayalamSans)
      echo "2.0.1" "67" "This is RaghuMalayalamSans, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language." "GPL2"
      ;;
    Suruma)
      echo "6.1.1" "67" "This is Suruma, a font belonging to a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language." "GPL3"
      ;;
    *)
      echo "0" "0" "Random"
      ;;
  esac
}

## End of font details. Don't edit past this ##

if [ "$1" == "clean" ]; then
  echo "Cleaning up existing files"
  for FLAVOR in "${FLAVORS[@]}" 
  do
    rm -rf "$FLAVOR"
  done
fi

pkgbuildtemplate (){
  FULLNAME=$1
  NAME=`echo $FULLNAME| tr '[:upper:]' '[:lower:]'`
  PKGVER=$2
  CONFPREFIX=$3
  PKGDESC=$4
  if [ -e $5 ]; then 
    PKGREL=1
  else
    PKGREL=$6
  fi
 
#define the template.
cat << EOF
# Contributor: Aashik S  aashiks at gmail dot com
# Maintainer: http://smc.org.in
# Contributor: Akshay S Dinesh asdofindia at gmail dot com

pkgname=ttf-malayalam-font-$NAME
pkgver=$PKGVER
pkgrel=$PKGREL
pkgdesc="$PKGDESC"
arch=(any)
url="http://smc.org.in/fonts/"
license="GPL3"
depends=(fontconfig xorg-font-utils)
source=("http://smc.org.in/downloads/fonts/$NAME/$FULLNAME.ttf"
        "https://gitlab.com/smc/$NAME/raw/master/$CONFPREFIX-smc-$NAME.conf")
md5sums=('updpkgsums'
         'updpkgsums')
install=\$pkgname.install


package() {
  mkdir -p "${pkgdir}/usr/share/fonts/TTF" || return 1
  install -m644 *.ttf "${pkgdir}/usr/share/fonts/TTF" 
  mkdir -p "${pkgdir}/etc/fonts/conf.d" || return 1
  install *.conf "${pkgdir}/etc/fonts/conf.d" || return 1
}
EOF
}

installfiletemplate () {
cat << EOF
post_install() {
  echo "Running fc-cache to update font cache"
  fc-cache -fs
  mkfontscale usr/share/fonts/TTF
  mkfontdir usr/share/fonts/TTF
  echo "Finished updating font cache"
}

post_upgrade() {
  post_install
}

post_remove() {
  post_install
}
EOF
}

metatemplate () {
cat << EOF
# Contributor: Aashik S  aashiks at gmail dot com
# Maintainer: http://smc.org.in
# Contributor: Akshay S Dinesh asdofindia at gmail dot com

pkgname=ttf-malayalam-fonts-meta
pkgver=6.1
pkgrel=1
pkgdesc="This is a set of TrueType and OpenType fonts released under the GNU General Public License for Malayalam Language."
arch=(any)
url="http://smc.org.in/fonts/"

depends=(ttf-malayalam-font-anjalioldlipi ttf-malayalam-font-chilanka ttf-malayalam-font-dyuthi 
ttf-malayalam-font-keraleeyam ttf-malayalam-font-meera ttf-malayalam-font-rachana ttf-malayalam-font-raghumalayalamsans ttf-malayalam-font-suruma )

package(){
return 0
}
EOF
}

packagedir () {
  cd $1
  updpkgsums
  makepkg --source -f
  cd -
}

for FLAVOR in "${FLAVORS[@]}" 
do
  for FONT in "${FONTS[@]}"
  do
    NAME=`echo $FONT| tr '[:upper:]' '[:lower:]'`
    read PKGVER CONFPREFIX PKGDESC<<<$(fontdetails $FONT)
    mkdir -p $FLAVOR/ttf-malayalam-font-$NAME/
    pkgbuildtemplate $FONT $PKGVER $CONFPREFIX "$PKGDESC" > $FLAVOR/ttf-malayalam-font-$NAME/PKGBUILD
    installfiletemplate > $FLAVOR/ttf-malayalam-font-$NAME/ttf-malayalam-font-$NAME.install
    packagedir $FLAVOR/ttf-malayalam-font-$NAME/
  done
  mkdir -p $FLAVOR/ttf-malayalam-fonts-meta/
  metatemplate > $FLAVOR/ttf-malayalam-fonts-meta/PKGBUILD
  packagedir $FLAVOR/ttf-malayalam-fonts-meta/
done
